from selenium import webdriver

driver = webdriver.Chrome(executable_path='driver/mac/chromedriver')
driver.get('https://docs.python.org/3.8/glossary.html')

with open('js/test.js') as f:
    script_return = driver.execute_script(f.read())
    print(script_return)
