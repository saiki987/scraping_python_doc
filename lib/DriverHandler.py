from selenium import webdriver
import os
import time

class DriverHandler:
    def get_driver(self):
        return self.driver

    def open_browser(self):
        chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument("--window-size=1920,1080")
        # chrome_options.add_argument("--start-maximized")
        # chrome_options.add_argument("--headless")
        # ブラウザが起動して準備できるまで自動で待ってくれる
        driver_path = os.environ.get("DRIVER_PATH")
        driver = webdriver.Chrome(executable_path=driver_path,options=chrome_options)
        self.driver = driver

    def close_browser(self):
        self.driver.quit()
