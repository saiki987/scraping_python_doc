import json
from dotenv import load_dotenv
from lib.DriverHandler import DriverHandler

if __name__ == '__main__':
    load_dotenv('.env')
    driver_handler = DriverHandler()
    driver_handler.open_browser()
    driver = driver_handler.get_driver()

    # ページに移動して、
    driver.get('https://docs.python.org/3.8/glossary.html')
    # jsを読み込み、
    with open('js/terms.js', encoding='utf-8') as f:
        terms_script = f.read()
    # jsを実行し、データを取得
    terms = driver.execute_script(terms_script)

    driver_handler.close_browser()

    # データの書き込み
    with open(f'data/terms.json', 'w+', encoding="utf-8") as f:
        json.dump(terms, f, indent=4, ensure_ascii=False)
